#include <string.h>
#include <math.h>
#include "cg_kernels.h"
#include "SparseMatrix.h"
#include "finout.h"


void task_mv(int i, int b, int n, int nb, SparseMatrix *sm, double *p, double *z, double *z2) {
	int ub = n - i;
	ub = ( ub < b ) ? ub : b;

	int *r = &sm->vptr[i];
	z2 += (i/b) * n;

	memset(z2, 0, sizeof(double) * n);

	int ii;
	for ( ii =0 ; ii < ub; ii++ ) {
		int start = r[ii];
		int nz = r[ii+1] - start;
		--start;
		int *idx = &sm->vpos[start];
		double *vec = &sm->vval[start];

		// mkl_scsrmv() instead?
		// Set initial value for z[i:i+b-1,:] 
		//z[ii] = ddoti(&nz, vec, idx, p);
		z[ii] = ddoti(nz, vec, idx, p);

		// Transpose the rows, and update z2
		int nzmone = nz - 1;
		int pos = i + ii;
		//daxpyi(&nzmone, &p[pos], &vec[1], &idx[1], z2);
		daxpyi(nzmone, p[pos], &vec[1], &idx[1], z2);
	}
}

void task_zred(int i, int b, int n, int nb, double *z2, double *z) {
	int ione = 1;
	double done = 1.0;

	int ub = n - i;
	ub = (ub < b) ? ub : b;
	z2 += i;

	int j;
	for ( j = 0; j < nb; j++ ) {
		//daxpy(&ub, &done, z2, &ione, z, &ione);
		daxpy(ub, done, z2, z);
		z2 += n;
	}
}


void task_ddot_init(int b, double *p, double *z, double *dotp) {
	int ione = 1;

	//*dotp = ddot(&b, p, &ione, z, &ione);
	*dotp = ddot(b, p, z);
}

void task_ddot(int i, int b, int n, double *p, double *z, double *dotp) {
	int ione = 1;

	int ub = n - i;
	ub = (ub < b) ? ub : b;

	//*dotp += ddot(&ub, p, &ione, z, &ione);
	*dotp += ddot(ub, p, z);
}

void task_ddot_fin(double *gamma, int b, double *p, double *z, double *dotp) {
	int ione = 1;
	//double ldotp = *dotp + ddot(&b, p, &ione, z, &ione);
	double ldotp = *dotp + ddot(b, p, z);
	*dotp = *gamma / ldotp;
}


void task_axpy(int i, int b, int n, double *alpha, double *p, double *z, double *x, double *res) {
	int ione = 1;
	double lalpha = *alpha;

	int ub = n - i;
	ub = (ub < b) ? ub : b;

	//daxpy (&ub, &lalpha, p, &ione, x, &ione);
	daxpy(ub, lalpha, p, x);
	lalpha = -lalpha;
	//daxpy (&ub, &lalpha, z, &ione, res, &ione);
	daxpy(ub, lalpha, z, res);
}


void task_ddot2_init(int b, double *p, double *prev, double *dotp) {
	int ione = 1;
	*prev = *dotp;
	//*dotp = ddot(&b, p, &ione, p, &ione);
	*dotp = ddot(b, p, p);
}

void task_ddot2(int b, double *p, double *dotp) {
	int ione = 1;
	//*dotp += ddot(&b, p, &ione, p, &ione);
	*dotp += ddot(b, p, p);
}

void task_ddot2_fin(int b, double *p, double *dotp, double *beta, double *prevg, double *tol) {
	int ione = 1;
	//*dotp += ddot(&b, p, &ione, p, &ione);
	*dotp += ddot(b, p, p);
	*beta = *dotp / *prevg;
	*tol = sqrt(*dotp);
}


void task_scalaxpy(int i, int b, int n, double *beta, double *p, double *res) {
	int ione = 1;
	double done = 1.0;

	int ub = n - i;
	ub = (ub < b) ? ub : b;

	//dscal (&ub, beta, p, &ione);
	//daxpy (&ub, &done, res, &ione, p, &ione);
	dscal(ub, *beta, p);
	daxpy(ub, done, res, p);
}


