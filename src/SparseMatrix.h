/*
 * SparseMatrix.h
 *
 * CSR sparse matrix
 *
 *  Created on: Feb 5, 2016
 *      Author: Ferad Zyulkyarov
 */

#ifndef SPARSEMATRIX_H_
#define SPARSEMATRIX_H_

#include <stdio.h>
#include "SparseMatrixType.h"


extern FILE *OpenFile(char *name, char *attr);

extern void ReadStringFile(FILE *file, char *string, int length);

extern int CreateSparseMatrix(ptr_SparseMatrix spr, int numR, int numC, int numE, int msr);

extern void sparse_free(SparseMatrix *spr);

extern int CreateAndInitializeSparseMatrix(ptr_SparseMatrix spr, int numRows, int numCols);

extern void PrintSparseMatrix(SparseMatrix spr, int CorF);

extern int ProdSparseMatrixVector(SparseMatrix spr, double *vec, double *res);

extern int ProdSparseMatrixVectorBlock(SparseMatrix spr, double *vec, double *res);

extern int ProdSparseMatrixVectorEcc(SparseMatrix spr, double *vec, double *res);

extern int ProdSparseMatrixVectorBlockEcc(SparseMatrix spr, double *vec, double *res);

extern int ProdSymSparseMatrixVector(SparseMatrix spr, double *vec, double *res);

extern void CreateSparseMatrixHB(char *nameFile, ptr_SparseMatrix spr, int FtoC);

extern void dcopy(int n, double* rhs, double* lhs);
extern void daxpy(int n, double c, double* rhs, double* lhs);
extern double ddot(int n, double* a, double* b);
extern void dscal(int n, double c, double* arr);
extern double ddoti(int nz, double* sparseVect, int* idx, double* vect);
extern void daxpyi(int nz, double c, double* sparseVect, int* idx, double* vect);

#endif /* SPARSEMATRIX_H_ */
