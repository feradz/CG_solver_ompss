/*
 * custom_memalloc.h
 *
 *  Created on: Feb 16, 2016
 *      Author: Ferad Zyulkyarov
 */

#ifndef CUSTOM_MEMALLOC_H_
#define CUSTOM_MEMALLOC_H_

void *allocate( size_t size );

#endif /* CUSTOM_MEMALLOC_H_ */
