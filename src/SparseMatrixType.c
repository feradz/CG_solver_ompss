/*
 * SparseMatrixType.c
 *
 * Defines the accessor functions for ECC protected value structure.
 *
 *  Created on: Feb 15, 2016
 *      Author: Ferad Zyulkyarov
 */

#include "ecc.h"
#include "SparseMatrixType.h"

double get_value(EccProtectedValue* val_ecc)
{
   if (check_ecc(val_ecc->vval, val_ecc->ecc) != 1)
   {
      return (double)fail_task(val_ecc);
   }
   return val_ecc->vval;
}

void set_value(EccProtectedValue* val_ecc, double new_val)
{
   val_ecc->vval = new_val;
   val_ecc->ecc = ecc(new_val);
}

