#ifndef __CQ_SEQMAIN_H__
#define __CQ_SEQMAIN_H__


#include "SparseMatrix.h"


long cg(SparseMatrix *A, double *rhs, double *x, double *res, double *z, double *p, int b, int maxit);


#endif // __CQ_SEQMAIN_H__
