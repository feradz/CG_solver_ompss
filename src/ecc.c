/*
 * ecc.c
 *
 * This file defines the Error Checking and Correction (ECC)
 * prototype functions.
 *
 *  Created on: Feb 6, 2016
 *      Author: Ferad Zyulkyarov
 */

#include "ecc.h"
#include <mpoison.h>
#include <stdbool.h>

unsigned char ecc(double dval)
{
   return secded(dval);
}

unsigned char check_ecc(double num, unsigned ecc_val)
{
   return ecc(num) == ecc_val;
}

unsigned char fail_task(void* ptr) {
   unsigned char temp = 0;
   mpoison_block_page((uintptr_t) ptr);
   temp = *(unsigned char*)(ptr);
   return temp;
}

void flip_bit(double* num) {
   //
   // Bit flip the least significant bit.
   //
   unsigned long long mask = 0x1;
   unsigned long long* numi = (unsigned long long*)num;
   if (*numi & mask == 0) {
      *numi = *numi | mask;
   } else {
      *numi = *numi ^ mask;
   }

   mask = 0x8;
   if (*numi & mask == 0) {
      *numi = *numi | mask;
   } else {
      *numi = *numi ^ mask;
   }

   mask = 0x128;
   if (*numi & mask == 0) {
      *numi = *numi | mask;
   } else {
      *numi = *numi ^ mask;
   }
}

/**
 * This method computes SECDED (extended Hamming code) for 64 bit block of data.
 *
 * d - is the 64 bit data for which the SECDED code is to be computed.
 *
 * Returns 8 bit SECDED code.
 * Bits c[6:0] are the SEC parity bits
 * Bit c[7] is the parity for all the remaining bits.
 *
 * Bit   Checks these bits of d
 * c[0]  0, 1, 3, 5, ..., 63   (0 and the odd positions)
 * c[1]  0, 2-3, 6-7, ..., 62-63 (0 and the positions xxxx1x)
 * c[2]  0, 4-7, 12-15, 20-23, 28-31, 36-39, 44-47, 52-55, 60-63 (0 and the positions xxx1xx)
 * c[3]  0, 8-15, 24-31, 40-47, 56-63 (0 and the positions xx1xxx)
 * c[4]  0, 16-31, 48-63 (0 and the positions x1xxxx)
 * c[5]  0, 32-63 (0 and the positions 1xxxxx)
 * c[6]  1-63
 *
 */
unsigned char secded(double dval)
{
    unsigned long long* d = (unsigned long long*)(&dval);
    unsigned long long c0, c1, c2, c3, c4, c5, c6;
    unsigned long long s0, s1, s2, s3, s4, s5, s6;
    unsigned long long t1, t2, t3, t4;
    unsigned char parity_of_c = 0;
    unsigned char overall_parity = 0;
    unsigned char c = 0;
    unsigned char p = 0;

    //
    // First calculate c[6:0] ignoring d[0]
    //

    c0 = *d  ^ (*d  >> 2);
    c0 = c0 ^ (c0 >> 4);
    c0 = c0 ^ (c0 >> 8);
    c0 = c0 ^ (c0 >> 16);
    c0 = c0 ^ (c0 >> 32);   // c0 is in position 1

    t1 = *d  ^ (*d  >> 1);
    c1 = t1 ^ (t1 >> 4);
    c1 = c1 ^ (c1 >> 8);
    c1 = c1 ^ (c1 >> 16);
    c1 = c1 ^ (c1 >> 32);   // c1 is in position 2

    t2 = t1 ^ (t1 >> 2);
    c2 = t2 ^ (t2 >> 8);
    c2 = c2 ^ (c2 >> 16);
    c2 = c2 ^ (c2 >> 32);   // c2 is in position 4

    t3 = t2 ^ (t2 >> 4);
    c3 = t3 ^ (t3 >> 16);
    c3 = c3 ^ (c3 >> 32);   // c3 is in position 8

    t4 = t3 ^ (t3 >> 8);
    c4 = t3 ^ (t3 >> 32);   // c4 is in position 16

    c5 = t4 ^ (t4 >> 16);   // c5 is in position 32

    c6 = c5 ^ (c5 >> 32);   // c6 is in position 0

    s0 = (c0 >> 1) & 1;
    s1 = (c1 >> 1) & 2;
    s2 = (c2 >> 2) & 4;
    s3 = (c3 >> 5) & 8;
    s4 = (c4 >> 12) & 16;
    s5 = (c5 >> 27) & 32;
    s6 = (c6 & 1) << 6; // parity of d
    c = s0 | s1 | s2 | s3 | s4 | s5 | s6;

    //
    // Now account for d[0]. Because d[0] was included in the computation of c6, we take the complement of it -(d&1)
    //
    c = c ^ (-(*d & 1) & 0x7F);

    // Compute the parity for c
    parity_of_c = c ^ (c >> 1);
    parity_of_c = parity_of_c ^ (parity_of_c >> 2);
    parity_of_c = parity_of_c ^ (parity_of_c >> 4);
    parity_of_c = parity_of_c & 1;
    overall_parity = parity_of_c ^ c6; // The combined parity of data d and the check bits c
    overall_parity = (overall_parity & 1) << 7;

    p = c | overall_parity;
    return p;
}

/**
 *
 * BUG
 * When the data is 1 and the modified data is 0 (1 bit flip from 1 to 0)
 * p0 = 0, syn != 0 then the execution falls to this condition
 *
 * Correction has bugs. Does not work properly.
 *
 * This function looks at the received 8 checkbits and the 64 data bits and
 * determines how many errors occurred. It is assumed that the errors can be 0, 1 or 2.
 * It returns 0, 1 and 2. 0 means no errors. 1 means one error and 2 means two errors.
 * The function corrects the data word if there was one error.
 *
 * The most significant bit of p i.e. p[7] is the overall parity (for the checkbits+data)
 */
unsigned char secded_correct(unsigned char p, double *dval)
{
    unsigned long long *d = (unsigned long long*)dval;
    unsigned char pn, syn, b, c, cn;
    unsigned long long po;

    c = p & 0x7F; // Get the checkbits only from p

    // Calculate the overall parity
    po = c ^ *d;
    po = po ^ (po >> 1);
    po = po ^ (po >> 2);
    po = po ^ (po >> 4);
    po = po ^ (po >> 8);
    po = po ^ (po >> 16);
    po = po ^ (po >> 32);
    po = po & 1;

    // Calculate the checkbits
    pn = secded(*d);
    cn = pn & 0x7F;

    // Syndrome exclusive the overall parity
    syn = cn ^ c;

    if (po == 0)
    {
        if (syn == 0)
        {
            // If no errors return 0.
            return 0;
        }
        else
        {
            //
            // TODO Bug
            // When the data is 1 and the modified data is 0 (1 bit flip from 1 to 0)
            // p0 = 0, syn != 0 then the execution falls to this condition
            //
            //

            // Two errors return 2;
            return 2;
        }
    }

    if (((syn - 1) && syn) == 0)
    {
        //
        // One error occurred in the check bits.
        // If syn has zero or one bits set, then the error is int the
        // checkbits or the overall parity bit. No correction is required.
        //
        return 1;
    }

    //
    // One error occurred and the syn bits 6:0 tell where its in d.
    //

    // Map syn to range 0 to 63.
    b = syn - 63 - (syn >> 6);

    //
    // This conditional block is equivalent to the statement above.
    //
    //if (syn == 0x3F)
    //{
    //  b = 0;
    //}
    //else
    //{
    //  b = syn & 0x3F;
    //}

    *d = *d ^ (1 << b);
    return 1;
}
