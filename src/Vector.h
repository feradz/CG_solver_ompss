/*
 * Vector.h
 *
 * Declares function prototypes for vector operations.
 *
 *  Created on: Feb 5, 2016
 *      Author: Ferad Zyulkyarov
 */

#ifndef VECTOR_H_
#define VECTOR_H_

#include "SparseMatrixType.h"

extern int CreateInts (int **vint, int num);

extern int InitInts (int *vint, int n, int frst, int incr);

extern int CopyShiftInts (int *src, int *dst, int n, int shft);

extern void GetIntFromString (char *string, int *pnum, int numC, int shft);

extern void GetIntsFromString (char *string, int *vec, int numN, int numC, int shft);

extern void GetFormatsFromString (char *string, int *vec, int numN, int numC);

extern int PrintInts (int *vint, int num);

extern int RemoveInts (int **vint);

extern int PrintDoubles (double *vdouble, int num);

extern int CreateAndInitializeDoubleVector(double** vector, int numElements);


extern int InitDoubles (double *vdouble, int n, double frst, double incr);

extern void GetDoubleFromString (char *string, double *pdbl, int numC);

extern void GetDoublesFromString (char *string, double *vec, int numN, int numC);


extern int InitEccProtectedValue (EccProtectedValue *vdouble, int n, double frst, double incr);

extern void GetEccProtectedValueFromString (char *string, EccProtectedValue *pspval, int numC);

extern void GetEccProtectedValuesFromString (char *string, EccProtectedValue *vec, int numN, int numC);


#endif /* VECTOR_H_ */
