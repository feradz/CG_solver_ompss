#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "Vector.h"
#include "SparseMatrix.h"
#include "finout.h"
#include "cg_main.h"


int main (int argc, char *argv[]) {
	if ( argc < 2) {
		fprintf(stderr, "use: %s HBfile [b] [maxit] [rep]\n", argv[0]);
		return 1;
	}

	SparseMatrix mat;
	CreateSparseMatrixHB (argv[1], &mat, 1);
	int n = mat.dim1;
	int maxit = n;
	int b = n;
	int rep = 1;

	if ( argc > 2 ) {
		b = atoi(argv[2]);
		b = (b > n) ? n : b;
		if ( argc > 3 ) {
			maxit = atoi(argv[3]);
			if ( maxit > n ) {
				fprintf(stderr, "warning: setting maxit to %i\n", maxit);
			}
			if ( argc > 4 ) {
				rep = atoi(argv[4]);
			}
		}
	}


	double *x = (double* ) malloc(sizeof(double) * n);
	if ( x == NULL ) {
		fprintf(stderr, "err: could not allocate x\n");
		return 1;
	}
	InitDoubles (x, n, 1.0, 0.0);
	double *rhs = (double*) malloc(sizeof(double) * n);;
	if ( rhs == NULL ) {
		fprintf(stderr, "err: could not allocate rhs\n");
		return 1;
	}
	InitDoubles (rhs, n, 0.0, 0.0);

	CopyShiftInts (mat.vpos, mat.vpos, mat.vptr[n], 1);
	CopyShiftInts (mat.vptr, mat.vptr, n+1, 1);
	//mkl_dcsrsymv ("U", &n, mat.vval, mat.vptr, mat.vpos, x, rhs);
	ProdSymSparseMatrixVector(mat, x, rhs);
	sparse_print_CSC(argv[1], &mat, rhs);

	InitDoubles (x, n, 0.0, 0.0);

	double *res = (double*) malloc(sizeof(double) * n);
	if ( res == NULL ) {
		fprintf(stderr, "error: could not allocate res\n");
		return 1;
	}
	double *z = (double*) malloc(sizeof(double) * n);
	if ( z == NULL ) {
		fprintf(stderr, "error: could not allocate z\n");
		return 1;
	}
	double *p = (double*) malloc(sizeof(double) * n);
	if ( p == NULL ) {
		fprintf(stderr, "error: could not allocate p\n");
		return 1;
	}

	long elapsed = 0;
	int i;
	for ( i=0; i<rep; i++) {
		elapsed += cg(&mat, rhs, x, res, z, p, b, maxit);
	}

	double flelapsed=(double)elapsed;
	printf("time : %.2f\n", flelapsed / (double) rep);


	free(rhs);
	free(x);
	sparse_free(&mat);
	free(res);
	free(z); 
	free (p);

	return 0;
}
