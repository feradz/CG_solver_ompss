#ifndef __FINOUT_H__
#define __FINOUT_H__


#include <stdio.h>


extern FILE* create_cscf(char *name);
extern void sparse_print_CSC (char *file, ptr_SparseMatrix A, double *b);
extern void print_dvector (char *fname, char *name, int n, double *v);
extern void sparse_print_col(char *f, char *id, SparseMatrix *A, int col);


#endif // __FINOUT_H__
