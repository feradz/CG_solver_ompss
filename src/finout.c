#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>


#include "Vector.h"
#include "SparseMatrix.h"
#include "finout.h"


static FILE *cscf=NULL;

FILE* create_cscf(char *name) {
	if ( cscf==NULL) {
		size_t len = strlen(name);
		char newfile[len+4+1];
		strcpy(newfile, name);
		newfile[len]='.';
		newfile[len+1]='c';
		newfile[len+2]='s';
		newfile[len+3]='c';
		newfile[len+4]='\0';
		cscf = fopen(newfile, "w");
	}
	return cscf;
}

void sparse_print_CSC(char *file, ptr_SparseMatrix A, double *b) {
	int m = A->dim1;
	int n = A->dim2;
	int elemc = A->elemc;
	int *colptr = A->vptr;
	int *rowind = A->vpos;
	double *val = A->vval;

	FILE *f = create_cscf(file);
	//struct stat finfo;
	//if ( stat(file,&finfo) == 0 ) return;

	
	fprintf(f, "# name: r\n");
	fprintf(f, "# type: matrix\n");
	fprintf(f, "# rows: 1\n");
	fprintf(f, "# columns: %i\n", elemc);
	int j;
	for ( j=0; j<elemc; j++ ) {
		fprintf(f, " %i ", rowind[j]);
	}

	fprintf(f, "\n# name: c\n");
	fprintf(f, "# type: matrix\n");
	fprintf(f, "# rows: 1\n");
	fprintf(f, "# columns: %i\n", elemc);
	for ( j=0; j<n; j++ ) {
		int vc = colptr[j+1] - colptr[j]; 
		int jj;
		for ( jj=0; jj<vc; jj++) {
			fprintf(f, " %i ", j+1);
		}
	}

	fprintf(f, "\n# name: v\n");
	fprintf(f, "# type: matrix\n");
	fprintf(f, "# rows: 1\n");
	fprintf(f, "# columns: %i\n", elemc);
	for ( j=0; j<elemc; j++ ) {
		fprintf(f, " %.16f ", val[j]);
	}

	fprintf(f, "\n# name: b\n");
	fprintf(f, "# type: matrix\n");
	fprintf(f, "# rows: 1\n");
	fprintf(f, "# columns: %i\n", n);
	for ( j=0; j<n; j++ ) {
		fprintf(f, " %.16f ", b[j]);
	}
}


void print_dvector (char *fname, char *name, int n, double *v) {
	FILE *fstrm = fopen(fname, "a");
	if ( fstrm == NULL ) {
		fprintf(stderr, "warning: failed to open %s\n", fstrm);
	}

	fprintf(fstrm, "\n# name: %s\n", name);
	fprintf(fstrm, "# type: matrix\n");
	fprintf(fstrm, "# rows: 1\n");
	fprintf(fstrm, "# columns: %i\n", n);

	int i;
	for (i=0; i<n; i++) {
		fprintf(fstrm, " %.16f ", v[i]);
	}

	fprintf(fstrm, "\n");
	fclose(fstrm);
}


// I assume A is in CSC format, and that only the lower-diagonal 
// part is represented.
void sparse_print_col(char *f, char *id, SparseMatrix *A, int col) {
	FILE *fstrm = fopen(f,"a");
	if ( fstrm == NULL ) {
		fprintf(stderr, "warning: failed to open %s\n", f);
	}

	int n = A->dim1;
	int fcol = col + 1;

	int *vptr = A->vptr;
	int *vpos = A->vpos;
	double *vval = A->vval;

	fprintf(fstrm, "# name: %s\n", id);
	fprintf(fstrm, "# type: matrix\n");
	fprintf(fstrm, "# rows: 1\n");
	fprintf(fstrm, "# columns: %i\n", n);

	// First get the elements above the diagonal
	int j;
	for ( j=0; j<col; j++ ) {
		int elemc = vptr[j+1] - vptr[j];
		int start = vptr[j] - 1;

		int i;
		for ( i=0; i<elemc; i++ ) {
			if ( vpos[start+i] == fcol ) {
				fprintf(fstrm, " %f ", vval[start+i]);		
				break;
			}
		}
		if ( i == elemc ) { 
			fprintf(fstrm, " 0.0 ");
		};
	}

	// Then the elements on or below the diagonal
	int elemc = vptr[col+1] - vptr[col];
	int start = vptr[col] - 1;
	int c = 0;
	int i;
	for ( i=col; i<n; i++ ) {
		int idx = vpos[start+c] - 1;
		double res = 0.0;
		if (idx == i ) { 
			res=vval[start+c];
			++c;
		}
		fprintf(fstrm, " %f ", res);		
	}
	fprintf(fstrm, "\n");

	fclose(fstrm);
}
