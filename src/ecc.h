/*
 * ecc.h
 *
 * This file declares the Error Checking and Correction (ECC)
 * prototype functions.
 *
 *  Created on: Feb 6, 2016
 *      Author: Ferad Zyulkyarov
 */

#ifndef ECC_H_
#define ECC_H_

/**
 * Calculates ECC for double number
 * @param num the number to calculate the ECC.
 * @return the ECC for the number.
 */
unsigned char ecc(double num);

/**
 * Checks the ECC for the number.
 * @param num the number to check the ECC for.
 * @param ecc_val the ECC to compare with.
 * @return true (1) if the computed ECC matches the provided ecc, otherwise false (0).
 */
unsigned char check_ecc(double num, unsigned ecc_val);

/**
 * Fail task.
 * @param ptr a pointer to the input of a task.
 */
unsigned char fail_task(void* ptr);

/**
 * Calculates SECDED ECC.
 * @param num the number to calculate SECDED for
 * @return the SECDED code.
 */
unsigned char secded(double num);

/**
 * Cecks how many bits are wrong. If 1 bit flipped, it corrects the bit value.
 * @param ecc the precomputed SECDED ECC for the provided value.
 * @param val the current value
 * @return It returns 0, 1 and 2 the number of bits that flipped.
 */
unsigned char secded_correct(unsigned char ecc, double *val);

/**
 * Inserts a bit flip.
 * @param num the number where a bit flip has to be inserted.
 */
void flip_bit(double* num);

#endif /* ECC_H_ */
