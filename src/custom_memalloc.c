/*
 * custom_memalloc.c
 *
 *  Created on: Feb 16, 2016
 *      Author: Ferad Zyulkyarov
 */

#include "custom_memalloc.h"

void *allocate( size_t size ) {
   static const size_t page_size = 4096;
   if (size < page_size) {
      size = page_size;
   }

   void *ptr;
   posix_memalign(&ptr, page_size, size);
   return ptr;
}
