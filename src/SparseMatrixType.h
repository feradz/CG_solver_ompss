/*
 * SparseMatrixType.h
 *
 *  Created on: Feb 10, 2016
 *      Author: ferad
 */

#ifndef SPARSEMATRIXTYPE_H_
#define SPARSEMATRIXTYPE_H_

typedef struct {
   double vval;
   unsigned char ecc;
} EccProtectedValue, *ptr_EccProtectedValue;

typedef struct {
   int dim1, dim2;
   int elemc;
   int *vptr;
   int *vpos;
   EccProtectedValue* vval;
} SparseMatrix, *ptr_SparseMatrix;

/**
 * Returns the value of ECC protected value structure.
 * @param val_ecc
 * @return the value.
 */
double get_value(EccProtectedValue* val_ecc);

/**
 * Sets the value for ECC protected value structure.
 * @param val_ecc the ECC protected value.
 * @param new_val the new value to be set.
 */
void set_value(EccProtectedValue* val_ecc, double new_val);

#endif /* SPARSEMATRIXTYPE_H_ */
