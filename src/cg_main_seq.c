#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/time.h>
#include "cg_main.h"
#include "Vector.h"
#include "SparseMatrix.h"
#include "finout.h"


long cg(SparseMatrix *A, double *rhs, double *x, double *res, double *z, double *p, int b, int maxit) {
	int IONE = 1; 
	double DONE = 1.0, DMONE = -1.0;

	int n = A->dim1; 
	double *mat = A->vval;
	int *vptr = A->vptr;
	int *vpos = A->vpos;

	double umbral = 1.0e-8;

	struct timeval start;
	gettimeofday(&start,NULL);
	
	//mkl_dcsrsymv ("U", &n, mat, vptr, vpos, x, z);            // z = A * x
	ProdSymSparseMatrixVector(*A, x, z);                        // z = A * x (A is symmetric)
	//dcopy (&n, rhs, &IONE, res, &IONE);                       // res = b
	dcopy(n, rhs, res);                                         // res = rhs
	//daxpy (&n, &DMONE, z, &IONE, res, &IONE);                 // res -= z
	daxpy(n, DMONE, z, res);                                    // res -= z
	//dcopy(&n, res, &IONE, p, &IONE);                          // p = res
	dcopy(n, res, p);                                           // p = res
	//double gamma = ddot (&n, res, &IONE, res, &IONE);         // gamma = res' * res
	double gamma = ddot(n, res, res);                           // gamma = res' * res
	double tol = sqrt (gamma);                                  // tol = sqrt(gamma) = norm (res)
	int iter = 0;
	while ((iter < maxit) && (tol > umbral)) {
		print_dvector ("seq.log", "p", n, p);
		//mkl_dcsrsymv ("U", &n, mat, vptr, vpos, p, z);        // z = A * p
		ProdSymSparseMatrixVector(*A, p, z);                    // z = A * p
		print_dvector ("seq.log", "z", n, z);
		//double alpha = gamma / ddot (&n, p, &IONE, z, &IONE); // alpha = gamma / (p' * z)
		double alpha = gamma / ddot(n, p, z);                   // alpha = gamma / (p' * z)
		// daxpy (&n, &alpha, p, &IONE, x, &IONE);              // x += alpha * p
		daxpy(n, alpha, p, x);                                  // x += alpha * p
		alpha = -alpha;
		//daxpy (&n, &alpha, z, &IONE, res, &IONE);             // res -= alpha * z
		daxpy(n, alpha, z, res);                                // res -= alpha * z

		alpha = gamma;                                          // reuse alpha ... alpha = gamma
		//gamma = ddot (&n, res, &IONE, res, &IONE);            // gamma = res' * res
		gamma = ddot(n, res, res);                              // gamma = res' * res
		double beta = gamma / alpha;                            // alpha = gamma / alpha
		//dscal (&n, &beta, p, &IONE);                          // p = beta * p
		dscal(n, beta, p);                                      // p = beta * p
		//daxpy (&n, &DONE, res, &IONE, p, &IONE);              // p += res
		daxpy(n, DONE, res, p);                                 // p += res
		tol = sqrt (gamma);                                     // tol = sqrt(gamma) = norm (res)
		iter++;
		//printf ("iter %i tol %e\n",iter, tol);
	}

	struct timeval stop;
	gettimeofday(&stop,NULL);

	printf ("it %i res %e\n", iter, tol);

	unsigned long elapsed = (stop.tv_sec - start.tv_sec) * 1000000;
	elapsed += (stop.tv_usec - start.tv_usec);

	return elapsed;
} 
