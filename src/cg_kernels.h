#ifndef __CG_KERNELS_H__
#define __CG_KERNELS_H__


#include "SparseMatrix.h"


#pragma omp task in([n]p) out([b]z, [nb*n]z2)
void task_mv(int i, int b, int n, int nb, SparseMatrix *sm, double *p, double *z, double *z2);

#pragma omp task in([nb*n]z2) inout([b]z)
void task_zred(int i, int b, int n, int nb, double *z2, double *z);


#pragma omp task in([b]p, [b]z) out([1]dotp)
void task_ddot_init(int b, double *p, double *z, double *dotp); 

#pragma omp task in([b]p, [b]z) inout([1]dotp)
void task_ddot(int i, int b, int n, double *p, double *z, double *dotp); 

#pragma omp task in([b]p, [b]z, [1]gamma) inout([1]dotp) 
void task_ddot_fin(double *gamma, int b, double *p, double *z, double *dotp); 


#pragma omp task in([1]alpha, [b]p, [b]z) inout([b]x, [b]res) 
void task_axpy(int i, int b, int n, double *alpha, double *p, double *z, double *x, double *res); 


#pragma omp task in([b]p, [1]prev) out([1]dotp)
void task_ddot2_init(int b, double *p, double *prev, double *dotp);  


#pragma omp task in([b]p) inout([1]dotp)
void task_ddot2(int b, double *p, double *dotp);  


#pragma omp task in([1]prevg, [b]p) inout([1]dotp) out([1]beta, [1]tol)
void task_ddot2_fin(int b, double *p, double *dotp, double *beta, double *prevg, double *tol);  


#pragma omp task in([1]beta, [b]res) inout([b]p)
void task_scalaxpy(int i, int b, int n, double *beta, double *p, double *res);  


#endif // __CG_KERNELS_H__
