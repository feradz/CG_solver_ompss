/*
 * Vector.c
 *
 *  Implements vector.
 *
 *  Created on: Feb 9, 2016
 *      Author: Ferad Zyulkyarov
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "custom_memalloc.h"
#include "Vector.h"
#include "ecc.h"

/**
 * Allocates an array of integers.
 * @param vint pointer to the array of integers.
 * @param num number of elements.
 * @return
 */
int CreateInts(int **vint, int num) {
   if ((*vint = (int *) malloc(sizeof(int) * num)) == NULL) {
      printf("Memory Error (CreateInts(%d))\n", num);
      exit(1);
   }

   return 0;
}

/**
 * Initializes and array of ints.
 * @param vint
 * @param n
 * @param frst
 * @param incr
 * @return
 */
int InitInts(int *vint, int n, int frst, int incr) {
   int i, *p1 = vint, num = frst;

   for (i = 0; i < n; i++) {
      *(p1++) = num;
      num += incr;
   }

   return 0;
}

/**
 * Copies by shifting the values of the ints.
 * @param src
 * @param dst
 * @param n
 * @param shft
 * @return
 */
int CopyShiftInts(int *src, int *dst, int n, int shft) {
   int i, *p1 = src, *p2 = dst;

   for (i = 0; i < n; i++)
      *(p2++) = *(p1++) + shft;

   return 0;
}

/**
 * Parses an integer from a string.
 * @param string
 * @param pnum
 * @param numC
 * @param shft
 */
void GetIntFromString(char *string, int *pnum, int numC, int shft) {
   int j = 0, num = 0, neg = 0;
   char *pchar = string;

   while ((j < numC) && ((*pchar < '0') || (*pchar > '9')) && (*pchar != '+')
         && (*pchar != '-')) {
      j++;
      pchar++;
   }
   if (j < numC) {
      if ((*pchar == '+') || (*pchar == '-')) {
         neg = (*pchar == '-');
         j++;
         pchar++;
      }
      while ((j < numC) && (*pchar >= '0') && (*pchar <= '9')) {
         num = num * 10 + (*pchar - 48);
         j++;
         pchar++;
      }
   }
   if (neg)
      num = -num;
   *pnum = num + shft;
}

/**
 * Parses multiple integers from an HB file line.
 * @param string
 * @param vec
 * @param numN
 * @param numC
 * @param shft
 */
void GetIntsFromString(char *string, int *vec, int numN, int numC, int shft) {
   int i, *pint = vec;
   char *pchar = string;

   for (i = 0; i < numN; i++) {
      GetIntFromString(pchar, (pint++), numC, shft);
      pchar += numC;
   }
}

void GetFormatsFromString(char *string, int *vec, int numN, int numC) {
   int i, k = 0;
   int *pint = vec;
   char *pchar = string, *pch = NULL, c = ' ', c2;

   for (i = 0; i < numN; i++) {
      pch = pchar;
      while (*pch == ' ')
         pch++;
      sscanf(pch, "(%i%c", pint, &c);
      if ((c == 'P') || (c == 'p')) {
         sscanf(pch, "(%i%c%i%c%i.%i)", &k, &c2, pint, &c, pint + 1, pint + 2);
         pint += 3;
      } else if ((c == 'E') || (c == 'e') || (c == 'D') || (c == 'd')
            || (c == 'F') || (c == 'f') || (c == 'G') || (c == 'g')) {
         sscanf(pch, "(%i%c%i.%i)", pint, &c, pint + 1, pint + 2);
         pint += 3;
      } else {
         sscanf(pch, "(%i%c%i)", pint, &c, pint + 1);
         pint += 2;
      }
      pchar += numC;
   }
}

/**
 * Prints ints.
 * @param vint
 * @param num
 * @return
 */
int PrintInts(int *vint, int num) {
   int i, *pi = vint;

   for (i = 0; i < num; i++)
      printf("%d ", *(pi++));
   printf("\n");

   return 0;
}

/**
 * Prints doubles.
 * @param vint
 * @param num
 * @return
 */
int PrintDoubles(double *vdouble, int num) {
   int i;

   for (i = 0; i < num; i++)
      printf("%f ", vdouble[i]);
   printf("\n");

   return 0;
}

/**
 * Initializes an array of doubles.
 * @param vdouble
 * @param n
 * @param frst
 * @param incr
 * @return
 */
int InitDoubles(double *vdouble, int n, double frst, double incr) {
   int i;
   double *p1 = vdouble, num = frst;

   for (i = 0; i < n; i++) {
      *(p1++) = num;
      num += incr;
   }

   return 0;
}

int CreateAndInitializeDoubleVector(double** vector, int numElements) {
   int i;
   double* temp_vector;
   *vector = (double*)allocate((sizeof(double) * numElements));
   if (*vector == NULL) {
      fprintf(stderr, "Error: cannot allocate memory for vector.\n");
      return 1;
   }

   temp_vector = *vector;
   for (i = 0; i < numElements; i++) {
      temp_vector[i] = (double)i;
   }
   return 0;
}

/**
 * Parses a double value from an HB string.
 * @param string
 * @param pdbl
 * @param numC
 */
void GetDoubleFromString(char *string, double *pdbl, int numC) {
   int j, k, exp, neg;
   double num, frac;
   char *pchar = string;

   j = 0;
   exp = 0;
   neg = 0;
   num = 0.0;
   frac = 1.0;
   while ((j < numC) && ((*pchar < '0') || (*pchar > '9')) && (*pchar != '+')
         && (*pchar != '-') && (*pchar != '.')) {
      j++;
      pchar++;
   }
   if (j < numC) {
      if ((*pchar == '+') || (*pchar == '-')) {
         neg = (*pchar == '-');
         j++;
         pchar++;
      }
      if (j < numC) {
         if (*pchar != '.')
            while ((j < numC) && (*pchar >= '0') && (*pchar <= '9')) {
               num = num * 10 + (*pchar - 48);
               j++;
               pchar++;
            }
         if (j < numC) {
            if (*pchar == '.') {
               j++;
               pchar++;
               while ((j < numC) && (*pchar >= '0') && (*pchar <= '9')) {
                  frac /= 10;
                  num += (*pchar - 48) * frac;
                  j++;
                  pchar++;
               }
            }
            if (neg)
               num = -num;
            if (j < numC) {
               if ((*pchar == 'e') || (*pchar == 'E') || (*pchar == 'd')
                     || (*pchar == 'D')) {
                  neg = 0;
                  j++;
                  pchar++;
                  if (j < numC) {
                     if ((*pchar == '+') || (*pchar == '-')) {
                        neg = (*pchar == '-');
                        j++;
                        pchar++;
                     }
                     if (j < numC) {
                        while ((j < numC) && (*pchar >= '0') && (*pchar <= '9')) {
                           exp = exp * 10 + (*pchar - 48);
                           j++;
                           pchar++;
                        }
                        if (neg)
                           exp = -exp;
                        for (k = 0; k < exp; k++)
                           num *= 10;
                        for (k = 0; k > exp; k--)
                           num /= 10;
                     }
                  }
               }
            }
         } else if (neg)
            num = -num;
      }
   }
   *pdbl = num;
}

/**
 * Parses multiple double values from a line in HB file.
 * @param string
 * @param vec
 * @param numN
 * @param numC
 */
void GetDoublesFromString(char *string, double *vec, int numN, int numC) {
   int i;
   double *paux = vec;
   char *pchar = string;

   for (i = 0; i < numN; i++) {
      GetDoubleFromString(pchar, (paux++), numC);
      pchar += numC;
   }
}

/**
 * Initializes an array of sparse matrix values.
 * @param val an array of sparse matrix values
 * @param n
 * @param frst
 * @param incr
 * @return
 */
int InitEccProtectedValue(EccProtectedValue *val, int n, double frst, double incr) {
   int i;
   double num = frst;

   for (i = 0; i < n; i++) {
      set_value(&val[i], num);
      num += incr;
   }

   return 0;
}

/**
 * Parses a sparse matrix value from an HB string.
 * @param string
 * @param pspval
 * @param numC
 */
void GetEccProtectedValueFromString(char *string, EccProtectedValue *pspval, int numC) {
   double num = 0;
   GetDoubleFromString(string, &num, numC);
   set_value(pspval, num);
}

/**
 * Parses multiple sparse matrix values from a line in HB file.
 * @param string
 * @param vec
 * @param numN
 * @param numC
 */
void GetEccProtectedValuesFromString(char *string, EccProtectedValue *vec, int numN, int numC) {
   int i;
   EccProtectedValue *paux = vec;
   char *pchar = string;

   for (i = 0; i < numN; i++) {
      GetEccProtectedValueFromString(pchar, (paux++), numC);
      pchar += numC;
   }
}

