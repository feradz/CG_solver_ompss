#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/time.h>

#include "cg_main.h"
#include "Vector.h"
#include "SparseMatrix.h"
#include "finout.h"
#include "cg_kernels.h"


long cg(SparseMatrix *A, double *rhs, double *x, double *res, double *z, double *p, int b, int maxit) {
	int n = A->dim1; 
	double *mat = A->vval;
	int *vptr = A->vptr;
	int *vpos = A->vpos;
	int nb = (n + b - 1) / b;
	int l = n - (nb-1) * b;
	int bfin = (b==n)? 0 : l;
	int ifin = (b==n)? 0 : n - l;

	double *ztmp = (double*) calloc(n*nb, sizeof(double));
	if ( ztmp == NULL ) { 
		fprintf(stderr, "error: could not allocate ytmp\n");
		return 1;
	}


	int IONE = 1; 
	double DONE = 1.0, DMONE = -1.0;

	struct timeval start;
	gettimeofday(&start,NULL);

	//mkl_dcsrsymv ("U", &n, mat, vptr, vpos, x, z);       // z = A * x
	ProdSymSparseMatrixVector(*A, x, z);                   // z = A * x (A is symmetric)
	//dcopy (&n, rhs, &IONE, res, &IONE);                  // res = b
	dcopy(n, rhs, res);                                    // res = rhs
	//daxpy (&n, &DMONE, z, &IONE, res, &IONE);            // res -= z
	daxpy(n, DMONE, z, res);                               // res -= z
	//dcopy (&n, res, &IONE, p, &IONE);                    // p = res
	dcopy(n, res, p);                                      // p = res
	//double gamma = ddot (&n, res, &IONE, res, &IONE);    // gamma = res' * res
	double gamma = ddot(n, res, res);                      // gamma = res' * res
	double prevgamma;
	double tol = sqrt (gamma);                             // tol = sqrt(gamma) = norm (res)
	double umbral = 1.0e-8;
	double alpha=1.0;
	double beta;
	int iter = 0;
	while ((iter < maxit) && (tol > umbral)) {
		int i; 
		// z = A * p
		for ( i = 0; i < n; i+=b ) {
			task_mv(i, b, n, nb, A, p, &z[i], ztmp);
		}
		for ( i = 0; i < n; i+=b ) {
			task_zred(i, b, n, nb, ztmp, &z[i]);
		}

		// alpha = gamma / (p' * z)
		task_ddot_init(b, p, z, &alpha); 
		for ( i=b; i<n-b; i+=b ) {
			task_ddot(i, b, n, &p[i], &z[i], &alpha); 
		}
		task_ddot_fin(&gamma, bfin, &p[ifin], &z[ifin], &alpha); 

		// x += alpha * p;
		// res -= alpha * z
		for ( i=0; i<n; i+=b ) {
			task_axpy(i, b, n, &alpha, &p[i], &z[i], &x[i], &res[i]); 
		}

		// gamma = res' * res
		// beta = gamma / alpha
		// tol = sqrt(gamma) = norm (res)
		task_ddot2_init(b, res, &prevgamma, &gamma); 
		for ( i=b; i<n-b; i+=b ) {
			task_ddot2(b, &res[i], &gamma); 
		}
		task_ddot2_fin(bfin, &res[ifin], &gamma, &beta, &prevgamma, &tol); 

		// p = beta * p
		// p += res
		for ( i=0; i<n; i+=b ) {
			task_scalaxpy(i, b, n, &beta, &p[i], &res[i]);
		}

		iter++;

		#pragma omp taskwait on(tol)

		//printf ("iter %i tol %e\n",iter, tol);
	}

	struct timeval stop;
	gettimeofday(&stop,NULL);

	printf ("it %i res %e\n", iter, tol);

	free(ztmp);

	unsigned long elapsed = (stop.tv_sec - start.tv_sec) * 1000000;
	elapsed += (stop.tv_usec - start.tv_usec);

	return elapsed;
} 
