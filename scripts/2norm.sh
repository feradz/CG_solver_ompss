#!/bin/bash

f1=${1}
f2=${2}

n1=$( basename "${f1}" )
n1=${n1%.mm}
n2=$( basename "${f2}" )
n2=${n2%.mm}

ret=$( octave --no-window-system --silent --eval "load(\"${f1}\"); load(\"${f2}\"); norm(${n1} - ${n2})" )
echo ${ret#ans = }
