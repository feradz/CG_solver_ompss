#!/bin/bash

it=${1}

for i in $( seq 1 ${it} ) ; do 
	./cgbau 128 4 4 4 1 16 &> bla  

	nrm=$( ./2norm.sh x_16.mm test/refx_16.mm ) 
	if [ $? -ne 0 ]; then
		exit 1
	fi
	if [ "${nrm}" == "NaN" ]; then
		exit 2;
	fi

	echo ${nrm}

	rm bla
done ;
